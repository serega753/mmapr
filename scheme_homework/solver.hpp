//
// Created by sergey on 12.12.19.
//

#ifndef SCHEME_LAB_SOLVER_HPP
#define SCHEME_LAB_SOLVER_HPP


#include <vector>
#include <tuple>
#include <fstream>
#include "math_model.hpp"

class Solver {
	public:
		Solver(double epsilon, int max_n, double delta_t_min, double t_max);
		std::tuple<std::vector<double>, std::vector<double>> Solve();
		// first is solution, second - x_space
		~Solver() = default;
	private:
		double countIntegrPrecision();
		bool correctDeltaT(double integr_precision);
		bool NewtonMethod();

		MathModel math_model;
		std::shared_ptr<std::ostream> logger;

		std::vector<double> solution_;
		std::vector<double> t_space_;

		std::vector<double> prev_basis_;
		std::vector<double> cur_basis_;
		std::vector<double> pre_prev_basis_;

		double current_t_;
		double epsilon_;
		double delta_t_;
		double delta_t_prev_;
		const int max_n_;
		const double delta_t_min_;
		const double t_max_;

		static constexpr double eps_min_ = 1e-3;
		static constexpr double eps_max = 1;
};


#endif //SCHEME_LAB_SOLVER_HPP
