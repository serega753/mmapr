#include <iostream>

#include "solver.hpp"
#include "plotter.hpp"

int main() {
	constexpr double epsilon = 1e-1;
	constexpr int max_n = 7;
	constexpr double delta_t_min = 1e-9;
	constexpr double modelling_time = 1e-3;
	Solver solver( epsilon, max_n, delta_t_min, modelling_time);
	auto result = solver.Solve();
	Plotter plotter;
	const std::string data_name = "phi_5";
	plotter.AddPlot(std::get<0>(result), std::get<1>(result), data_name);
	plotter.Show();
	return 0;
}