#ifndef _GNUPLOT_H_
#define _GNUPLOT_H_

#include <cstdio>
#include <string>
#include <iostream>

#ifdef WIN32
#define GNUPLOT_NAME "pgnuplot -persist"
#else
#define GNUPLOT_NAME "gnuplot -persist"
#endif

using std::string;
using std::cerr;

class Gnuplot {
public:
	Gnuplot();
	~Gnuplot();
	void operator ()(const string & command);
protected:
	FILE *gnuplotpipe;
};

#endif // #ifndef _GNUPLOT_H_