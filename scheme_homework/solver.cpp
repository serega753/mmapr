//
// Created by sergey on 12.12.19.
//

#include <algorithm>
#include <sstream>
#include <cmath>
#include <iostream>

#include "solver.hpp"
#include "utils.hpp"

Solver::Solver(double epsilon, int max_n, double delta_t_min, double t_max):
		epsilon_(epsilon), max_n_(max_n), delta_t_min_(delta_t_min), t_max_(t_max), delta_t_(delta_t_min),
		delta_t_prev_(delta_t_min),
		current_t_(0.), math_model(), prev_basis_(MathModel::GetBasisSize()),
		cur_basis_(MathModel::GetBasisSize()), pre_prev_basis_(MathModel::GetBasisSize()) {}

bool Solver::NewtonMethod() {
	bool stop = false;
	bool success = false;
	int cur_n = 0;
	std::function<double(double, double)> get_initial_guess = [](double a, double b) -> double {
		return 2. * b - a;
	};
	cur_basis_ = vector_func<std::vector<double>&, std::vector<double>&>(prev_basis_.size(),
			get_initial_guess, pre_prev_basis_, prev_basis_);
	while(!stop) {
		auto system = math_model.GetNextSLAE(cur_basis_, prev_basis_, delta_t_, current_t_);
		system->Solve();
		auto deltas = system->GetSolution();
		for (int i = 0; i < cur_basis_.size(); ++i) {
			cur_basis_[i] += (*deltas)[i];
		}
		auto max_elem = std::fabs(*std::max_element(deltas->begin(), deltas->end(), [](double a, double b) {
			return std::fabs(a) < std::fabs(b);
		}));
		if (max_elem < epsilon_) {
			stop = true;
			success = true;
		} else {
			++cur_n;
			if (cur_n > max_n_) {
				stop = true;
				success = false;
			}
		}

		std::cout  << "Iteration number " << cur_n << " MAX DELTA IS " << max_elem << " " << std::endl;
	}
	return success;
}

std::tuple<std::vector<double>, std::vector<double>> Solver::Solve() {
	while (current_t_ < t_max_) {
		std::cout  << "t = " << current_t_ << std::endl;
		bool success = NewtonMethod();		// here success is that newton method converged
		std::cout  << "Newton method " << (success ? "converged" : "failed") << std::endl;
		if (success) {
			auto integr_precision = countIntegrPrecision();
			std::cout  << "Integrating precision = " << integr_precision << std::endl;
			bool integr_success = correctDeltaT(integr_precision);
			if (integr_success) {
				pre_prev_basis_ = prev_basis_;
				prev_basis_ = cur_basis_;
				solution_.push_back(cur_basis_[Indices::Phi5]);
				t_space_.push_back(current_t_);
			}
		} else {
			delta_t_prev_ = delta_t_;
			delta_t_ /= 2;
		}
		std::cout  << "delta_t = " << delta_t_ << std::endl;
		if (delta_t_ < delta_t_min_) {
			std::stringstream ss;
			ss << "Newton method failed with t = " << current_t_;
			throw std::runtime_error(ss.str());
		}
	}
	return std::make_tuple(solution_, t_space_);
}

double Solver::countIntegrPrecision() {
	auto delta = delta_t_;
	auto prev_delta = delta_t_prev_;
	std::function<double(double, double, double)> second_derivative = [delta, prev_delta](double x0,
			double x1, double x2) -> double {
		return fabs((x2 - x1) * delta / (delta + prev_delta) - (x1 - x0) * delta / prev_delta);
	};

	auto derivative_approx = vector_func<std::vector<double>&, std::vector<double>&>(cur_basis_.size(),
			second_derivative, pre_prev_basis_, prev_basis_, cur_basis_);
	auto result = std::fabs(*std::max_element(derivative_approx.begin() + Indices::Uc1, derivative_approx.end(), [](double a, double b) {
		return std::fabs(a) < std::fabs(b);
	}));
	return result;
}

bool Solver::correctDeltaT(double integr_precision) {
	if (std::fabs(integr_precision) < eps_min_) {
		current_t_ += delta_t_;
		std::cout  << "==========================" << std::endl;
		delta_t_prev_ = delta_t_;
		delta_t_ *= 2.;
		return true;
	}

	if (std::fabs(integr_precision) <= eps_max) {
		current_t_ += delta_t_;
		std::cout  << "==========================" << std::endl;
		return true;
	}
	delta_t_prev_ = delta_t_;
	delta_t_ /= 2.;
	return false;
}
