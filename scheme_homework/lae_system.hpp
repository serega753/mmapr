//
// Created by sergey on 19.05.19.
//

#ifndef LAB1_LAE_SYSTEM_HPP
#define LAB1_LAE_SYSTEM_HPP

#include <vector>
#include <memory>

class LAESystem {
	public:
		virtual void Solve() = 0;
		virtual std::shared_ptr<std::vector<double>> GetSolution() = 0;
		virtual void Print() = 0;
		virtual ~LAESystem() = default;
};

class GaussLAESystem: public LAESystem {
	public:
		GaussLAESystem(std::vector<std::vector<double>>&& matrix, std::vector<double>&& free_vector);
		GaussLAESystem() = default;
		~GaussLAESystem() override = default;
		void Solve() override;
		std::shared_ptr<std::vector<double>> GetSolution() override ;
		GaussLAESystem(GaussLAESystem&) = default;
		GaussLAESystem(GaussLAESystem&&) = default;
		GaussLAESystem& operator=(GaussLAESystem const&) = default;
		GaussLAESystem& operator=(GaussLAESystem&&) = default;

		void Print() override ;	// remove

	private:
		void makeTriangle();
		void backTrace();
		int findMaxElem(int index);
		void swapLines(int first, int second);

		std::vector<std::vector<double>> matrix_;
		std::vector<double> free_vector_;
		std::vector<double> solution_;
};

#endif //LAB1_LAE_SYSTEM_HPP
