//
// Created by sergey on 26.11.19.
//

#include <sstream>
#include "plotter.hpp"

void Plotter::AddPlot(const std::vector<double>& solution, const std::vector<double>& x_space, const std::string& data_name) {
	if (solution.size() != x_space.size()) {
		throw std::invalid_argument("sizes don't match");
	}
	out_stream << "set style data line" << std::endl;
	out_stream << plot_name << plot_num << "<< EOD" << std::endl;
	for (int i = 0; i < solution.size(); ++i) {
		out_stream << x_space[i] << " " << solution[i] << std::endl;
	}
	plot_names.push_back(data_name);
	out_stream << "EOD" << std::endl;
	++plot_num;
}


void Plotter::Show() {
	out_stream << "plot ";
	out_stream << plot_name << 0 << " " << "title \"" << plot_names[0] << "\"";
	for (int i = 1; i < plot_num; ++i) {
		out_stream << ", " << plot_name << i << " with linespoints " << "title \"" << plot_names[i] << "\"";
	}
	auto res = out_stream.str();
	plot(res);
}
