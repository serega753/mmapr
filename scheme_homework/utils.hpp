//
// Created by sergey on 12.12.19.
//

#ifndef SCHEME_LAB_UTILS_HPP
#define SCHEME_LAB_UTILS_HPP

#include <vector>
#include <functional>

template <class... VectorArgs, class... Args>
std::vector<double> vector_func(const int vector_size, std::function<double(Args...)> func, VectorArgs&... args) {
	std::vector<double> result;
	for (int i = 0; i < vector_size; ++i) {
		result.push_back(func(args[i]...));
	}
	return result;
}

extern template
std::vector<double> vector_func(int vector_size, std::function<double(double)> func, std::vector<double>&);
extern template
std::vector<double> vector_func(int vector_size, std::function<double(double, double)> func, std::vector<double>&,
								std::vector<double>&);
extern template
std::vector<double> vector_func(int vector_size, std::function<double(double, double, double)> func,
								std::vector<double>&, std::vector<double>&, std::vector<double>&);

#endif //SCHEME_LAB_UTILS_HPP
