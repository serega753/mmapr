//
// Created by sergey on 19.05.19.
//

#include <iostream>
#include <cmath>
#include "lae_system.hpp"

GaussLAESystem::GaussLAESystem(std::vector<std::vector<double >>&& matrix, std::vector<double>&& free_vector):
												matrix_(matrix), free_vector_(free_vector) {}


int GaussLAESystem::findMaxElem(int index) {
	int max = index;
	for (int i = index; i < matrix_.size(); ++i) {
		if (std::fabs(matrix_[i][index]) > std::fabs(matrix_[max][index])) {
			max = i;
		}
	}
	return max;
}

void GaussLAESystem::swapLines(int first, int second) {
	for (int i = 0; i < matrix_.size(); ++i) {
		std::swap(matrix_[first][i], matrix_[second][i]);
	}
	std::swap(free_vector_[first], free_vector_[second]);
}

void GaussLAESystem::makeTriangle() {
	for (int i = 0; i < matrix_.size(); ++i) {
		int max_index = findMaxElem(i);
		if (max_index != i) {
			swapLines(i, max_index);
		}
		for (int j = i + 1; j < matrix_.size(); ++j) {
			double coeff = matrix_[j][i] / matrix_[i][i];
			for (int k = i; k < matrix_.size(); ++k) {
				matrix_[j][k] -= coeff * matrix_[i][k];
			}
			free_vector_[j] -= coeff * free_vector_[i];
		}
	}

}

void GaussLAESystem::backTrace() {
	solution_.assign(matrix_.size(), 0);
	for (int i = matrix_.size() - 1; i >= 0; --i) {
		solution_[i] = free_vector_[i];
		for (int j = i + 1; j < matrix_.size(); ++j) {
			solution_[i] -= matrix_[i][j] * solution_[j];
		}
		solution_[i] /= matrix_[i][i];
	}
}

void GaussLAESystem::Solve() {
	makeTriangle();
	backTrace();
}

std::shared_ptr<std::vector<double>> GaussLAESystem::GetSolution() {
	return std::make_shared<std::vector<double>>(solution_);
}

void GaussLAESystem::Print() {
	std::cout << "Matrix" << std::endl;
	for (auto& it : matrix_) {
		for (auto& it2 : it) {
			std::cout.setf(std::ios::left);
			std::cout.width(10);
			std::cout << it2 << " ";
		}
		std::cout << std::endl;
	}
	std::cout << "Vector" << std::endl;
	for (auto& it : free_vector_) {
		std::cout << it << std::endl;
	}
}
