//
// Created by sergey on 12.12.19.
//

#ifndef SCHEME_LAB_MATH_MODEL_HPP
#define SCHEME_LAB_MATH_MODEL_HPP


#include "lae_system.hpp"

enum Indices {
	DerivUc1 = 0,
	DerivUc2 = 1,
	DerivUc3 = 2,
	DerivUc4 = 3,
	DerivIl1 = 4,
	Uc1 = 5,
	Uc2 = 6,
	Uc3 = 7,
	Uc4 = 8,
	Il1 = 9,
	Phi1 = 10,
	Phi2 = 11,
	Phi3 = 12,
	Phi4 = 13,
	Phi5 = 14,
	Ie = 15
};

class MathModel {
	public:
		MathModel() = default;
		std::unique_ptr<LAESystem> GetNextSLAE(const std::vector<double>& cur_basis,
				const std::vector<double>& prev_basis, double delta_t, double cur_time);
		~MathModel() = default;

		static int GetBasisSize() { return basis_size;}

	private:

		double evaluateDiodCurrent(double It, double phi_left, double phi_right);
		double evaluateDiodCurrentDerivativeLeft(double It, double phi_left, double phi_right);
		double evaluateDiodCurrentDerivativeRight(double It, double phi_left, double phi_right);
		double evaluateSinVoltageSource(double cur_time);

		static constexpr int basis_size =16;

		static constexpr double L1 = 2.53e-4;
		static constexpr double C1 = 1.e-6;
		static constexpr double E1_amplitude = 10;
		static constexpr double E1_period = 1.e-4;

		static constexpr double mFt = 0.026;
		// first diod
		static constexpr double R2 = 1.e6;
		static constexpr double C2 = 2.e-12;
		static constexpr double It1 = 1.e-12;
		static constexpr double R1 = 20.;

		// second diod
		static constexpr double R4 = 1.e6;
		static constexpr double C3 = 2.e-12;
		static constexpr double It2 = 1.e-12;
		static constexpr double R3 = 20.;

		static constexpr double C4 = 1.e-6;
		static constexpr double R5 = 1.e3;
};



#endif //SCHEME_LAB_MATH_MODEL_HPP
