//
// Created by sergey on 12.12.19.
//

#include <cmath>

#include "math_model.hpp"

std::unique_ptr<LAESystem> MathModel::GetNextSLAE(const std::vector<double> &cur_basis,
		const std::vector<double> &prev_basis, const double delta_t, const double cur_time) {

	std::vector<double> residual_vector = {
			// derivatives
			cur_basis[Indices::DerivUc1] - (cur_basis[Indices::Uc1] - prev_basis[Indices::Uc1]) * (1. / delta_t),
			cur_basis[Indices::DerivUc2] - (cur_basis[Indices::Uc2] - prev_basis[Indices::Uc2]) * (1. / delta_t),
			cur_basis[Indices::DerivUc3] - (cur_basis[Indices::Uc3] - prev_basis[Indices::Uc3]) * (1. / delta_t),
			cur_basis[Indices::DerivUc4] - (cur_basis[Indices::Uc4] - prev_basis[Indices::Uc4]) * (1. / delta_t),
			cur_basis[Indices::DerivIl1] - (cur_basis[Indices::Il1] - prev_basis[Indices::Il1]) * (1. / delta_t),

			// state values
			cur_basis[Indices::Uc1] - (cur_basis[Indices::Phi1] - cur_basis[Indices::Phi2]),
			cur_basis[Indices::Uc2] - (cur_basis[Indices::Phi3] - cur_basis[Indices::Phi2]),
			cur_basis[Indices::Uc3] - (cur_basis[Indices::Phi4] - cur_basis[Indices::Phi5]),
			cur_basis[Indices::Uc4] - cur_basis[Indices::Phi5],
			L1 * cur_basis[Indices::DerivIl1] - (cur_basis[Indices::Phi1] - cur_basis[Indices::Phi2]),

			// node #1
			cur_basis[Indices::Ie] + cur_basis[Indices::Il1] + C1 * cur_basis[Indices::DerivUc1],
			// node #2
			-cur_basis[Indices::Il1] - C1 * cur_basis[Indices::DerivUc1] +
			(cur_basis[Indices::Phi2] - cur_basis[Indices::Phi4]) * (1. / R3)
			- C2 * cur_basis[Indices::DerivUc2] - evaluateDiodCurrent(It1, cur_basis[Indices::Phi3], cur_basis[Indices::Phi2])
			- (cur_basis[Indices::Phi3] - cur_basis[Indices::Phi2]) * (1. / R2),
			// node #3
			evaluateDiodCurrent(It1, cur_basis[Indices::Phi3], cur_basis[Indices::Phi2]) +
			C2 * cur_basis[Indices::DerivUc2] + (cur_basis[Indices::Phi3] - cur_basis[Indices::Phi2]) * (1. / R2) +		// here might be - phi3 / r1
			cur_basis[Indices::Phi3] * (1. / R1),
			// node #4
			-(cur_basis[Indices::Phi2] - cur_basis[Indices::Phi4]) * (1. / R3) + C3 * cur_basis[Indices::DerivUc3] +
			(cur_basis[Indices::Phi4] - cur_basis[Indices::Phi5]) * (1. / R4) +
			evaluateDiodCurrent(It2, cur_basis[Indices::Phi4], cur_basis[Indices::Phi5]),
			// node #5
			C4 * cur_basis[Indices::DerivUc4] + cur_basis[Indices::Phi5] * (1. / R5) -
			evaluateDiodCurrent(It2, cur_basis[Indices::Phi4], cur_basis[Indices::Phi5]) -
			(cur_basis[Indices::Phi4] - cur_basis[Indices::Phi5]) * (1. / R4) - C3 * cur_basis[Indices::DerivUc3],
			// Ie1
			evaluateSinVoltageSource(cur_time) - cur_basis[Indices::Phi1]
	};
	for (auto& it : residual_vector) {
		it *= -1;
	}

	std::vector<std::vector<double>> jacobi_matrix(GetBasisSize());
	for (auto& it: jacobi_matrix) {
		it = std::vector<double>(GetBasisSize());
	}

	// here we place all the derivatives
	for (int i = 0; i < Indices ::Uc1; ++i) {
		jacobi_matrix[i][i] = 1.;
		jacobi_matrix[i][i + Indices::Uc1] = -1. / delta_t;
	}

	// Uc1 line
	jacobi_matrix[Indices::Uc1][Indices::Uc1] = 1.;
	jacobi_matrix[Indices::Uc1][Indices::Phi1] = -1.;
	jacobi_matrix[Indices::Uc1][Indices::Phi2] = 1.;

	// Uc2 line
	jacobi_matrix[Indices::Uc2][Indices::Uc2] = 1.;
	jacobi_matrix[Indices::Uc2][Indices::Phi3] = -1.;
	jacobi_matrix[Indices::Uc2][Indices::Phi2] = 1.;


	// Uc3 line
	jacobi_matrix[Indices::Uc3][Indices::Uc3] = 1.;
	jacobi_matrix[Indices::Uc3][Indices::Phi4] = -1.;
	jacobi_matrix[Indices::Uc3][Indices::Phi5] = 1.;

	// Uc4 line
	jacobi_matrix[Indices::Uc4][Indices::Uc4] = 1.;
	jacobi_matrix[Indices::Uc4][Indices::Phi5] = -1.;

	// Il line
	jacobi_matrix[Indices::Il1][Indices::DerivIl1] = L1;
	jacobi_matrix[Indices::Il1][Indices::Phi1] = -1.;
	jacobi_matrix[Indices::Il1][Indices::Phi2] = 1.;

	// Phi1 line
	jacobi_matrix[Indices::Phi1][Indices::Il1] = 1.;
	jacobi_matrix[Indices::Phi1][Indices::Ie] = 1.;
	jacobi_matrix[Indices::Phi1][Indices::DerivUc1] = C1;

	// Phi2 line
	jacobi_matrix[Indices::Phi2][Indices::DerivUc1] = -C1;
	jacobi_matrix[Indices::Phi2][Indices::Il1] = -1.;
	jacobi_matrix[Indices::Phi2][Indices::DerivUc2] = -C2;
	jacobi_matrix[Indices::Phi2][Indices::Phi2] = (1. / R3) + (1. / R2) -
			evaluateDiodCurrentDerivativeRight(It1, cur_basis[Indices::Phi3], cur_basis[Indices::Phi2]);
	jacobi_matrix[Indices::Phi2][Indices::Phi3] = (-1. / R2) -
			evaluateDiodCurrentDerivativeLeft(It1, cur_basis[Indices::Phi3], cur_basis[Indices::Phi2]);
	jacobi_matrix[Indices::Phi2][Indices::Phi4] = (-1. / R3);

	// Phi3 line
	jacobi_matrix[Indices::Phi3][Indices::DerivUc2] = C2;
	jacobi_matrix[Indices::Phi3][Indices::Phi2] = (-1. / R2) +
			evaluateDiodCurrentDerivativeRight(It1, cur_basis[Indices::Phi3], cur_basis[Indices::Phi2]);
	jacobi_matrix[Indices::Phi3][Indices::Phi3] = (1. / R1) + (1. / R2) + 	// here 1/r1 might be with minus
			evaluateDiodCurrentDerivativeLeft(It1, cur_basis[Indices::Phi3], cur_basis[Indices::Phi2]);

	// Phi4 line
	jacobi_matrix[Indices::Phi4][Indices::DerivUc3] = C3;
	jacobi_matrix[Indices::Phi4][Indices::Phi2] = -1. / R3;
	jacobi_matrix[Indices::Phi4][Indices::Phi4] = (1. / R3) + (1 / R4) +
			evaluateDiodCurrentDerivativeLeft(It2, cur_basis[Indices::Phi4], cur_basis[Indices::Phi5]);
	jacobi_matrix[Indices::Phi4][Indices::Phi5] = (-1. / R4) +
			evaluateDiodCurrentDerivativeRight(It2, cur_basis[Indices::Phi4], cur_basis[Indices::Phi5]);

	// Phi5 line
	jacobi_matrix[Indices::Phi5][Indices::DerivUc3] = -C3;
	jacobi_matrix[Indices::Phi5][Indices::DerivUc4] = C4;
	jacobi_matrix[Indices::Phi5][Indices::Phi4] = (-1. / R4) -
			evaluateDiodCurrentDerivativeLeft(It2, cur_basis[Indices::Phi4], cur_basis[Indices::Phi5]);
	jacobi_matrix[Indices::Phi5][Indices::Phi5] = (1. / R4) + (1. / R5) -
			evaluateDiodCurrentDerivativeRight(It2, cur_basis[Indices::Phi4], cur_basis[Indices::Phi5]);

	// Ie line
	jacobi_matrix[Indices::Ie][Indices::Phi1] = -1.;

	return std::make_unique<GaussLAESystem>(std::move(jacobi_matrix), std::move(residual_vector));
}

double MathModel::evaluateDiodCurrent(double It, double phi_in, double phi_out) {
	double u = phi_in - phi_out;
	double k = 2;
	double disp = 0.6;
	if (u < disp) {
		return 0;
	} else {
		return k * (u - disp);
	}
	return It * (exp((phi_in - phi_out) / mFt) - 1.);
}

double MathModel::evaluateDiodCurrentDerivativeLeft(double It, double phi_in, double phi_out) {
	double u = phi_in - phi_out;
	double k = 2;
	double disp = 0.6;
	if (u < disp) {
		return 0;
	} else {
		return k;
	}
	return It / mFt * (exp((phi_in - phi_out) / mFt));
}

double MathModel::evaluateDiodCurrentDerivativeRight(double It, double phi_in, double phi_out) {
	double u = phi_in - phi_out;
	double k = 2;
	double disp = 0.6;
	if (u < disp) {
		return 0;
	} else {
		return -k;
	}
	return -It / mFt * (exp((phi_in - phi_out) / mFt));
}

double MathModel::evaluateSinVoltageSource(double cur_time) {
	return  E1_amplitude * sin(cur_time * (2. * M_PI) / E1_period);
}
