//
// Created by sergey on 15.10.2020.
//

#include <limits>
#include <algorithm>
#include <iostream>

#include "lae_system.hpp"

static double eval_precision_cuda(std::vector<double>& matr, const int size, const double eps) {
	std::vector<double> sums(size);

	for (int i = 0; i < size; ++i) {
		for (int j = 0; j < size; ++j) {
			sums[i] += matr[i * size + j];
		}
	}
	auto norm = *std::max_element(sums.begin(), sums.end());
	if (norm < 0.5) {
		return eps;
	}

	return ((1 - norm) / norm) * eps;
}


YacobiCudaLAESystem::YacobiCudaLAESystem(
		std::vector<std::vector<double >>&& matrix,
		std::vector<double>&& free_vector,
		const double precision,
		const int blocks_num
):
		free_vector_(free_vector), eps_(precision), blocks_num_(blocks_num) {
			matrix_.reserve(matrix.size() * matrix.size());
			for (const auto& line: matrix) {
				for (const auto& elem: line) {
					matrix_.emplace_back(elem);
				}
			}
			residual_.assign(matrix.size(), std::numeric_limits<double>::infinity());
			solution_.assign(matrix.size(), 0);
}

void DealWithError(cudaError_t status, std::vector<double*>& to_free_dev) {
	for (auto& it: to_free_dev) {
		if(it) {
			cudaFree(it);
		}
	}
	throw std::runtime_error(cudaGetErrorString(status));
}

void YacobiCudaLAESystem::Solve() {
	auto size = free_vector_.size();
	for (int i = 0; i < size; ++i) {
		auto divider = matrix_[i * size + i];
		for (int j = 0; j < size; ++j) {
			if (i == j) {
				matrix_[i * size + j] = 0;
			} else {
				matrix_[i * size + j] /= -divider;
			}
		}
		free_vector_[i] /= divider;
	}
	solution_ = free_vector_;
	eps_ = eval_precision_cuda(matrix_, size, eps_);

	double* dev_matrix = nullptr;
	double* dev_free_vector = nullptr;
	double* dev_residual = nullptr;
	double* dev_solution = nullptr;
	double* dev_solution_prev = nullptr;

	std::vector<double*> vec_dev;

	auto err = cudaMalloc((void**)&dev_matrix, sizeof(double) * size * size);
	if (err != cudaSuccess) {
		DealWithError(err, vec_dev);
	}

	vec_dev.emplace_back(dev_matrix);
	err = cudaMalloc((void**)&dev_free_vector, sizeof(double) * size);
	if (err != cudaSuccess) {
		DealWithError(err, vec_dev);
	}
	vec_dev.emplace_back(dev_free_vector);

	err = cudaMalloc((void**)&dev_residual, sizeof(double) * size);
	if (err != cudaSuccess) {
		DealWithError(err, vec_dev);
	}
	vec_dev.emplace_back(dev_residual);

	err = cudaMalloc((void**)&dev_solution, sizeof(double) * size);
	if (err != cudaSuccess) {
		DealWithError(err, vec_dev);
	}
	vec_dev.emplace_back(dev_solution);

	err = cudaMalloc((void**)&dev_solution_prev, sizeof(double) * size);
	if (err != cudaSuccess) {
		DealWithError(err, vec_dev);
	}
	vec_dev.emplace_back(dev_solution_prev);

	err = cudaMemcpy(dev_matrix, matrix_.data(), sizeof(double) * size * size, cudaMemcpyHostToDevice);
	if (err != cudaSuccess) {
		DealWithError(err, vec_dev);
	}

	err = cudaMemcpy(dev_free_vector, free_vector_.data(), sizeof(double) * size, cudaMemcpyHostToDevice);
	if (err != cudaSuccess) {
		DealWithError(err, vec_dev);
	}

	err = cudaMemcpy(dev_solution, solution_.data(), sizeof(double) * size, cudaMemcpyHostToDevice);
	if (err != cudaSuccess) {
		DealWithError(err, vec_dev);
	}

	err = cudaMemset(dev_solution_prev, 0, sizeof(double) * size);
	if (err != cudaSuccess) {
		DealWithError(err, vec_dev);
	}

	err = cudaMemcpy(dev_residual, residual_.data(), sizeof(double) * size, cudaMemcpyHostToDevice);
	if (err != cudaSuccess) {
		DealWithError(err, vec_dev);
	}

	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	cudaEventRecord(start,nullptr);

	while (getNorm() > eps_) {
		makeStep(dev_matrix, dev_free_vector, dev_residual, dev_solution, dev_solution_prev);
	}
	err = cudaMemcpy(solution_.data(), dev_solution, sizeof(double) * size, cudaMemcpyDeviceToHost);
	if (err != cudaSuccess) {
		DealWithError(err, vec_dev);
	}

	float time_elapsed = 0;
	cudaEventRecord(stop, nullptr);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&time_elapsed, start, stop);
	cudaEventDestroy(start);
	cudaEventDestroy(stop);

	std::cout << "CUDA. It took " << time_elapsed << " ms" << std::endl;
	err = cudaFree(dev_matrix);
	if (err != cudaSuccess) {
		std::cerr << cudaGetErrorString(err) << std::endl;
	}
	err = cudaFree(dev_free_vector);
	if (err != cudaSuccess) {
		std::cerr << cudaGetErrorString(err) << std::endl;
	}

	err = cudaFree(dev_residual);
	if (err != cudaSuccess) {
		std::cerr << cudaGetErrorString(err) << std::endl;
	}

	err = cudaFree(dev_solution);
	if (err != cudaSuccess) {
		std::cerr << cudaGetErrorString(err) << std::endl;
	}
	err = cudaFree(dev_solution_prev);
	if (err != cudaSuccess) {
		std::cerr << cudaGetErrorString(err) << std::endl;
	}
}

std::shared_ptr<std::vector<double>> YacobiCudaLAESystem::GetSolution() {
	return std::make_shared<std::vector<double>>(solution_);
}

void YacobiCudaLAESystem::Print() {
	std::cout << "Matrix" << std::endl;
	auto size = free_vector_.size();
	for (int i = 0; i < size; ++i) {
		for (int j = 0; j < size; ++j) {
			std::cout.setf(std::ios::left);
			std::cout.width(10);
			std::cout << matrix_[i * size + j] << " ";
		}
		std::cout << std::endl;
	}
	std::cout << "Vector" << std::endl;
	for (auto& it : free_vector_) {
		std::cout << it << std::endl;
	}
}

double YacobiCudaLAESystem::getNorm() {
	return *std::max_element(residual_.begin(), residual_.end());
}

__global__ void get_next_x(double* dev_matrix, double* dev_free_vector, double* dev_residual,
						   double* dev_solution, double* dev_solution_prev, int size) {
	auto thread_id = threadIdx.x + blockIdx.x * blockDim.x;
	double sum = 0;
	if (thread_id < size) {
		for (int i = 0; i < size; ++i) {
			sum += dev_matrix[(thread_id * size) + i] * dev_solution_prev[i];
		}
		sum += dev_free_vector[thread_id];
		dev_residual[thread_id] = std::abs(sum - dev_solution_prev[thread_id]);
		dev_solution[thread_id] = sum;
	}
}

void YacobiCudaLAESystem::makeStep(double* dev_matrix, double* dev_free_vector, double* dev_residual,
		double* dev_solution, double* dev_solution_prev) {
	auto size = free_vector_.size();
	get_next_x<<<size / blocks_num_ + 1, blocks_num_>>>(dev_matrix, dev_free_vector,
			dev_residual, dev_solution, dev_solution_prev, size);
	cudaThreadSynchronize();
	auto err = cudaMemcpy(dev_solution_prev, dev_solution, sizeof(double) * size, cudaMemcpyDeviceToDevice);
	if (err != cudaSuccess) {
		std::cerr << cudaGetErrorString(err) << std::endl;
	}

	err = cudaMemcpy(residual_.data(), dev_residual, sizeof(double) * size, cudaMemcpyDeviceToHost);
	if (err != cudaSuccess) {
		std::cerr << cudaGetErrorString(err) << std::endl;
	}

}
