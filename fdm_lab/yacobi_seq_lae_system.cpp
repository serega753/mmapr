//
// Created by sergey on 21.09.2020.
//


#include <iostream>
#include <limits>

#include <omp.h>
#include <algorithm>

#include "lae_system.hpp"


YacobiSeqSystem::YacobiSeqSystem(
		std::vector<std::vector<double >>&& matrix,
		std::vector<double>&& free_vector,
		const double precision
):
		matrix_(matrix), free_vector_(free_vector), eps_(precision) {
	residual_.assign(matrix.size(), std::numeric_limits<double>::infinity());
	solution_.assign(matrix_.size(), 0);
}

std::shared_ptr<std::vector<double>> YacobiSeqSystem::GetSolution() {
	return std::make_shared<std::vector<double>>(solution_);
}

double YacobiSeqSystem::getNorm() {
	return *std::max_element(residual_.begin(), residual_.end());
}

void YacobiSeqSystem::makeStep() {
	for (int i = 0; i < matrix_.size(); ++i) {
		double sum = 0;
		for (int j = 0; j < matrix_.size(); ++j) {
			sum += matrix_[i][j] * solution_[j];
		}

		sum += free_vector_[i];
		residual_[i] = std::abs(sum - solution_[i]);
		solution_[i] = sum;
	}
}

void YacobiSeqSystem::Solve() {
	for (int i = 0; i < matrix_.size(); ++i) {
		auto divider = matrix_[i][i];
		for (int j = 0; j < matrix_.size(); ++j) {
			if (i == j) {
				matrix_[i][j] = 0;
			} else {
				matrix_[i][j] /= -divider;
			}
		}
		free_vector_[i] /= divider;
	}
	solution_ = free_vector_;
	eps_ = eval_precision(matrix_, eps_);
	auto t1 =omp_get_wtime();
	while (getNorm() > eps_) {
		makeStep();
	}
	auto t2 = omp_get_wtime();
	std::cout << "Sequential. It took " << t2 - t1 << " s" << std::endl;
}

void YacobiSeqSystem::Print() {
	std::cout << "Matrix" << std::endl;
	for (auto& it : matrix_) {
		for (auto& it2 : it) {
			std::cout.setf(std::ios::left);
			std::cout.width(10);
			std::cout << it2 << " ";
		}
		std::cout << std::endl;
	}
	std::cout << "Vector" << std::endl;
	for (auto& it : free_vector_) {
		std::cout << it << std::endl;
	}
}

