//
// Created by sergey on 20.05.19.
//

#ifndef LAB1_MATH_MODEL_HPP
#define LAB1_MATH_MODEL_HPP

#include <memory>

#include "lae_system.hpp"

class MathModel {
	public:
		explicit MathModel(int num_y);
		std::pair<std::vector<std::vector<double>>, std::vector<double>> operator()();

	private:

		int getPlaceInSolutionMatrix(int i, int j);

		int num_x_;		// number of nodes in OX
		int num_y_;		// number of nodes in OY
		double delta_x_;	// dist between 2 nodes in OX
		double delta_y_;	// dist between 2 nodes in OY
		std::shared_ptr<LAESystem> syst;

		static constexpr double x_len_ = 8;
		static constexpr double y_len_ = 4;
		static constexpr double left_cond_ = 200;
		static constexpr double right_cond_ = 40;
		static constexpr double top_cond_ = 100;
		static constexpr double down_cond_ = 100;
};


#endif //LAB1_MATH_MODEL_HPP
