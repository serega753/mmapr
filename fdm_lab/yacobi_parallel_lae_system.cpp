//
// Created by sergey on 20.09.2020.
//

#include <iostream>
#include <limits>

#include <omp.h>
#include <algorithm>

#include "lae_system.hpp"

double eval_precision(std::vector<std::vector<double>>& matr, const double eps) {
	std::vector<double> sums(matr.size());

	#pragma omp parallel for shared(matr, sums) default(none)
	for (int i = 0; i < matr.size(); ++i) {
		for (int j = 0; j < matr.size(); ++j) {
			sums[i] += matr[i][j];
		}
	}
	auto norm = *std::max_element(sums.begin(), sums.end());
	if (norm < 0.5) {
		return eps;
	}

	return ((1 - norm) / norm) * eps;
}

YacobiParallelSystem::YacobiParallelSystem(
			std::vector<std::vector<double >>&& matrix,
			std::vector<double>&& free_vector,
			const double precision
		):
			matrix_(matrix), free_vector_(free_vector), eps_(precision) {
				residual_.assign(matrix.size(), std::numeric_limits<double>::infinity());
				solution_.assign(matrix_.size(), 0);
}

std::shared_ptr<std::vector<double>> YacobiParallelSystem::GetSolution() {
	return std::make_shared<std::vector<double>>(solution_);
}

double YacobiParallelSystem::getNorm() {
	return *std::max_element(residual_.begin(), residual_.end());
}

void YacobiParallelSystem::makeStep() {
	#pragma omp parallel for shared(matrix_, free_vector_) default(none)
	for (int i = 0; i < matrix_.size(); ++i) {
		double sum = 0;
		for (int j = 0; j < matrix_.size(); ++j) {
			sum += matrix_[i][j] * solution_[j];
		}

		sum += free_vector_[i];
		residual_[i] = std::abs(sum - solution_[i]);
		solution_[i] = sum;
	}
}

void YacobiParallelSystem::Solve() {
	omp_set_dynamic(0);
	omp_set_num_threads(8);
	#pragma omp parallel for shared(matrix_, free_vector_) default(none)
	for (int i = 0; i < matrix_.size(); ++i) {
		auto divider = matrix_[i][i];
		for (int j = 0; j < matrix_.size(); ++j) {
			if (i == j) {
				matrix_[i][j] = 0;
			} else {
				matrix_[i][j] /= -divider;
			}
		}
		free_vector_[i] /= divider;
	}
	solution_ = free_vector_;
	eps_ = eval_precision(matrix_, eps_);
	auto t1 =omp_get_wtime();
	while (getNorm() > eps_) {
		makeStep();
	}
	auto t2 = omp_get_wtime();
	std::cout << "Parallel. It took " << t2 - t1 << " s" << std::endl;
}

void YacobiParallelSystem::Print() {
	std::cout << "Matrix" << std::endl;
	for (auto& it : matrix_) {
		for (auto& it2 : it) {
			std::cout.setf(std::ios::left);
			std::cout.width(10);
			std::cout << it2 << " ";
		}
		std::cout << std::endl;
	}
	std::cout << "Vector" << std::endl;
	for (auto& it : free_vector_) {
		std::cout << it << std::endl;
	}
}

