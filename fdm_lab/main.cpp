#include <iostream>
#include <sstream>

#include "lae_system.hpp"

#include "math_model.hpp"
#include "plotter.hpp"

int main(int argc, char* argv[]) {
	if (argc < 2) {
		throw std::invalid_argument("usage: <prog_name> y_nodes_num");
	}
	std::stringstream str(argv[1]);
	int nodes_num = 0;
	str >> nodes_num;
	MathModel get_lae(nodes_num);
	auto syst_tuple = get_lae();
	std::shared_ptr<LAESystem> syst = std::make_shared<YacobiCudaLAESystem>(std::move(syst_tuple.first),
																			 std::move(syst_tuple.second), 1e-2, 32);
	/*std::shared_ptr<LAESystem> syst = std::make_shared<YacobiParallelSystem>(std::move(syst_tuple.first),
																			 std::move(syst_tuple.second), 1e-2);*/
/*	std::shared_ptr<LAESystem> syst = std::make_shared<YacobiSeqSystem>(std::move(syst_tuple.first),
																			 std::move(syst_tuple.second), 1e-2);*/
//	syst->Print();
//	return 0;
	syst->Solve();
	auto solution = syst->GetSolution();
	std::cout << "SOLUTION IS HERE:" << std::endl;
	for (auto& elem : *solution) {
		std::cout << elem << std::endl;
	}
	Plotter plt(nodes_num);
	plt.Plot(solution);
	return 0;
}
