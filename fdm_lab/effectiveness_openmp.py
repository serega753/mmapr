import numpy as np
from matplotlib import pyplot as plt


def get_dim_by_y_nodes(y_nodes):
    return (y_nodes + y_nodes * 2) * y_nodes / 2


def main():
    threads_indices = np.array([1, 2, 4, 8])
    nodes_indices = np.array([np.int(get_dim_by_y_nodes(2 ** i)) for i in range(1, 6)])
    times = np.array([
        [1.464e-06,   2.9e-06,     3.09e-06,    7.881e-05],
        [0.0005,      0.0003,      0.0004,      0.0033],
        [0.0420545,   0.02223,     0.0136,      0.0321],
        [2.97032333,  1.4458,      0.8312,      1.0446],
        [197.501,     97.248,      52.635,      60.156]
    ])

    # time(threads_num)
    for i, nodes_num in enumerate(nodes_indices):
        x = threads_indices
        y = times[i, :]
        plt.rcParams['mathtext.fontset'] = 'stix'
        plt.rc('axes', labelsize=18)
        plt.plot(x, y, label=r'$N_{уравнений} = %d$' % nodes_num)
        plt.grid()
        plt.legend(loc='best', prop={'size': 18})
        plt.xlabel(r'$N_{потоков}$')
        plt.ylabel(r'$t, с$')
        plt.show()

    # time(nodes_num)
    for i, threads_num in enumerate(threads_indices):
        x = nodes_indices
        y = times[:, i]
        plt.rcParams['mathtext.fontset'] = 'stix'
        plt.rc('axes', labelsize=18)
        plt.plot(x, y, label=r'$N_{потоков} = %d$' % threads_num)
        plt.grid()
        plt.legend(loc='best', prop={'size': 18})
        plt.xlabel(r'$N_{уравнений}$')
        plt.ylabel(r'$t, с$')
        plt.show()

if __name__ == '__main__':
    main()