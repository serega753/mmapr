//
// Created by sergey on 19.05.19.
//

#ifndef LAB1_LAE_SYSTEM_HPP
#define LAB1_LAE_SYSTEM_HPP

#include <vector>
#include <memory>

double eval_precision(std::vector<std::vector<double>>& matr, double eps);

class LAESystem {
public:
	virtual void Solve() = 0;
	virtual std::shared_ptr<std::vector<double>> GetSolution() = 0;
	virtual void Print() = 0;
	virtual ~LAESystem() = default;
};


class GaussLAESystem: public LAESystem {
	public:
		GaussLAESystem(std::vector<std::vector<double>>&& matrix, std::vector<double>&& free_vector);
		GaussLAESystem() = default;
		~GaussLAESystem() override = default;
		void Solve() override;
		std::shared_ptr<std::vector<double>> GetSolution() override;
		GaussLAESystem(GaussLAESystem&) = default;
		GaussLAESystem(GaussLAESystem&&) = default;
		GaussLAESystem& operator=(GaussLAESystem const&) = default;
		GaussLAESystem& operator=(GaussLAESystem&&) = default;

		void Print() override;	// remove

	private:
		void makeTriangle();
		void backTrace();
		int findMaxElem(int index);
		void swapLines(int first, int second);

		std::vector<std::vector<double>> matrix_;
		std::vector<double> free_vector_;
		std::vector<double> solution_;
};


class YacobiParallelSystem: public LAESystem {
	public:
		YacobiParallelSystem(std::vector<std::vector<double>>&& matrix,
							 std::vector<double>&& free_vector, double precision);
		YacobiParallelSystem() = default;
		~YacobiParallelSystem() override = default;
		void Solve() override;
		std::shared_ptr<std::vector<double>> GetSolution() override;
		YacobiParallelSystem(YacobiParallelSystem&) = default;
		YacobiParallelSystem(YacobiParallelSystem&&) = default;
		YacobiParallelSystem& operator=(const YacobiParallelSystem&) = default;
		YacobiParallelSystem& operator=(YacobiParallelSystem&&) = default;

		void Print() override;	// remove

	private:

		double getNorm();
		void makeStep();


		std::vector<std::vector<double>> matrix_;
		std::vector<double> free_vector_;
		std::vector<double> solution_;
		double eps_;
//		double tau_;
		std::vector<double> residual_;
};


class YacobiSeqSystem: public LAESystem {
public:
	YacobiSeqSystem(std::vector<std::vector<double>>&& matrix,
						 std::vector<double>&& free_vector, double precision);
	YacobiSeqSystem() = default;
	~YacobiSeqSystem() override = default;
	void Solve() override;
	std::shared_ptr<std::vector<double>> GetSolution() override;
	YacobiSeqSystem(YacobiSeqSystem&) = default;
	YacobiSeqSystem(YacobiSeqSystem&&) = default;
	YacobiSeqSystem& operator=(YacobiSeqSystem&) = default;
	YacobiSeqSystem& operator=(YacobiSeqSystem&&) = default;

	void Print() override;	// remove

private:

	double getNorm();
	void makeStep();


	std::vector<std::vector<double>> matrix_;
	std::vector<double> free_vector_;
	std::vector<double> solution_;
	double eps_;
//		double tau_;
	std::vector<double> residual_;
};


class YacobiCudaLAESystem: public LAESystem {
public:
	YacobiCudaLAESystem(std::vector<std::vector<double>>&& matrix,
						std::vector<double>&& free_vector, double precision, int blocks_num);
	YacobiCudaLAESystem() = default;
	~YacobiCudaLAESystem() override = default;
	void Solve() override;
	std::shared_ptr<std::vector<double>> GetSolution() override;
	YacobiCudaLAESystem(YacobiCudaLAESystem&) = default;
	YacobiCudaLAESystem(YacobiCudaLAESystem&&) = default;
	YacobiCudaLAESystem& operator=(const YacobiCudaLAESystem&) = default;
	YacobiCudaLAESystem& operator=(YacobiCudaLAESystem&&) = default;

	void Print() override;	// remove

private:

	double getNorm();
	void makeStep(double* dev_matrix, double* dev_free_vector, double* dev_residual,
				  double* dev_solution, double* dev_solution_prev);


	std::vector<double> matrix_;
	std::vector<double> free_vector_;
	std::vector<double> solution_;
	double eps_;
	std::vector<double> residual_;
	int blocks_num_;

};


#endif //LAB1_LAE_SYSTEM_HPP
