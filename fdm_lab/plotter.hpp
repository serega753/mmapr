//
// Created by sergey on 21.05.19.
//

#ifndef LAB1_PLOTTER_HPP
#define LAB1_PLOTTER_HPP

#include <vector>

#include "gnuplot.h"

class Plotter {
	public:
		explicit Plotter(int num_y) : num_x_(num_y * 2 - 1), num_y_(num_y),
									 delta_x_(x_len_ / (num_y * 2 - 2)),
									 delta_y_(y_len_ / (num_y - 1)){}
	void Plot(std::shared_ptr<std::vector<double>>& decision);
	int getPlaceInSolutionMatrix(int y, int x);
	private:
		int num_x_;		// number of nodes in OX
		int num_y_;		// number of nodes in OY
		double delta_x_;	// dist between 2 nodes in OX
		double delta_y_;	// dist between 2 nodes in OY
		Gnuplot plot;
		static constexpr double x_len_ = 8;
		static constexpr double y_len_ = 4;
};


#endif //LAB1_PLOTTER_HPP
