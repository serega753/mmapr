//
// Created by sergey on 21.05.19.
//

#include <sstream>
#include <memory>
#include <fstream>

#include "plotter.hpp"

int Plotter::getPlaceInSolutionMatrix(int y, int x) {
	return static_cast<int>(static_cast<double>(num_x_ + num_x_ - y + 1) / 2.0
							* static_cast<double>(y) + static_cast<double>(x));
}

void Plotter::Plot(std::shared_ptr<std::vector<double>>& decision) {
	std::ofstream gp("gnuplot");
	for (int i = 0; i < num_y_; ++i) {
		for (int j = 0; j < num_x_ - i; ++j) {
			int index = getPlaceInSolutionMatrix(i, j);
			gp << j * delta_x_ << " " << i * delta_y_ << " " << (*decision)[index] << std::endl;
		}
		for (int j = num_x_ - i; j < num_x_; ++j) {
			int index = getPlaceInSolutionMatrix(i, j);
			gp << j * delta_x_ << " " << i * delta_y_ << " " << 0 << std::endl;
		}
	}
	plot("set view map");
	plot("set yrange [0:4]");
	plot("set xrange [0:8]");
	plot("set zrange [0:300]");
	plot("set dgrid 100,100,4");
	plot("set palette defined ( 0 0 0 1, 0.3333 0 1 0, 0.6667 1 0.6471 0, 1 1 0 0 )");
	plot("set autoscale fix");
	plot("splot 'gnuplot' u 1:2:3 with pm3d");
}
