//
// Created by sergey on 26.11.19.
//

#include "lin_elem_model.hpp"

std::shared_ptr<LAESystem> LinElemModel::operator()() {
	int equations_num = nodes_num_ + border_num;
	std::vector<std::vector<double>> matrix(equations_num);
	std::vector<double> free_vector(equations_num);
	for (int i = 0; i < equations_num; ++i) {
		std::vector<double> line(equations_num);
		matrix[i] = line;
	}
//	auto nodes = GetNodes();
//	elem_length_ = nodes[1] - nodes[0];
	elem_length_ = (end - start) / static_cast<double>(nodes_num_ - 1);
	MakeNewElemMatr();
	matrix[0][0] = elem_matrix_[0][0];
	matrix[0][1] = elem_matrix_[0][1];
	free_vector[0] = elem_vector_value_;
	for (int i = 1; i < nodes_num_ - 1; ++i) {
//		elem_length_ = nodes[i + 1] - nodes[i];
//		MakeNewElemMatr();
		matrix[i][i] = elem_matrix_[0][0] + elem_matrix_[1][1];
		matrix[i][i + 1] = elem_matrix_[0][1];
		matrix[i][i - 1] = elem_matrix_[1][0];
		free_vector[i] = 2 * elem_vector_value_;
	}

//	elem_length_ = *nodes.end() - *(nodes.end() - 1);
//	MakeNewElemMatr();
	matrix[nodes_num_ - 1][nodes_num_ - 1] = elem_matrix_[1][1];
	matrix[nodes_num_ - 1][nodes_num_ - 2] = elem_matrix_[1][0];
	free_vector[nodes_num_ - 1] = elem_vector_value_;

	// border conditions
	// u(x=2)=0
	matrix[0][matrix.size() - 2] = -a_coeff;
	matrix[matrix.size() - 2][0] = 1.;
	free_vector[free_vector.size() - 2] = on_start_val;

	// du/dx(14)=1
	matrix[nodes_num_ - 1][matrix.size() - 1] = a_coeff;
	matrix[matrix.size() - 1][matrix.size() - 1] = 1.;
	free_vector[free_vector.size() - 1] = on_end_val;

	system_ = std::make_shared<GaussLAESystem>(std::move(matrix), std::move(free_vector));
	return system_;
}

LinElemModel::LinElemModel(const int nodes_num): nodes_num_(nodes_num), elem_length_(length / static_cast<double>(nodes_num_)), elem_matrix_(), elem_vector_value_() {
}

void LinElemModel::MakeNewElemMatr() {
	elem_matrix_[0][0] = -a_coeff / elem_length_ + b_coeff * elem_length_ / 3.;
	elem_matrix_[0][1] = a_coeff / elem_length_ + b_coeff * elem_length_ / 6.;
	elem_matrix_[1][0] = a_coeff / elem_length_ + b_coeff * elem_length_ / 6.;
	elem_matrix_[1][1] = -a_coeff / elem_length_ + b_coeff * elem_length_ / 3.;
	elem_vector_value_ = -c_coeff * elem_length_ / 2.;
}

std::vector<double> LinElemModel::GetNodes() {
	return linspace<double>(start, end, nodes_num_);
}


