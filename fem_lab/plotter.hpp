//
// Created by sergey on 26.11.19.
//

#ifndef FEM_LAB_PLOTTER_HPP
#define FEM_LAB_PLOTTER_HPP


#include <vector>
#include <memory>
#include "gnuplot.h"

class Plotter {
	public:
		Plotter(): plot_num(0) {}
//		void Plot(std::vector<double>&, std::vector<double>&, std::string&);
		void AddPlot(std::vector<double>& solution, std::vector<double>& x_space, std::string& data_name);
		void Show();
		~Plotter() = default;

	private:
		Gnuplot plot;
		std::vector<std::string> plot_names;
		int plot_num;
		std::stringstream out_stream;
		static constexpr auto plot_name = "$data";

};


#endif //FEM_LAB_PLOTTER_HPP
