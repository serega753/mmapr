//
// Created by sergey on 26.11.19.
//

#ifndef FEM_LAB_QUAD_ELEM_MODEL_HPP
#define FEM_LAB_QUAD_ELEM_MODEL_HPP

#include "math_model.hpp"

class QuadElemModel: public MathModel {
	public:
		explicit QuadElemModel(int nodes_num);
		std::shared_ptr<LAESystem> operator()() override;
		~QuadElemModel() override = default;

		std::pair<double, double> GetBorders() const override { return {start, end};}

		std::vector<double> GetNodes() override;

	private:
		void MakeNewElemMatr();
		int nodes_num_;
		double elem_length_;
		std::shared_ptr<LAESystem> system_;
		std::array<std::array<double, 2>, 2> elem_matrix_;
		std::array<double, 2> elem_vector_;

		std::array<std::array<double, 3>, 3> initial_matrix_;
		std::array<double, 3> initial_vector_;

		static constexpr double a_coeff = 4;
		static constexpr double b_coeff = -9;
		static constexpr double c_coeff = 10;
		static constexpr int border_num = 2;
		static constexpr double start = 2;
		static constexpr double end = 14;
		static constexpr double length = end - start;

		// border conditions
		// du/dx(3)=1
		static constexpr double on_start_val = 0;
		// u(45)=10
		static constexpr double on_end_val = 1;
};


#endif //FEM_LAB_QUAD_ELEM_MODEL_HPP
