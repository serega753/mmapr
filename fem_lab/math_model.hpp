//
// Created by sergey on 26.11.19.
//

#ifndef FEM_LAB_MATH_MODEL_HPP
#define FEM_LAB_MATH_MODEL_HPP

#include "lae_system.hpp"

class MathModel {
	public:
		virtual std::shared_ptr<LAESystem> operator()() = 0;
		virtual std::pair<double, double> GetBorders() const = 0;
		virtual std::vector<double> GetNodes() = 0;
		virtual ~MathModel() = default;
};

template <typename T>
std::vector<T> linspace(T a, T b, size_t N);

template<>
std::vector<double> linspace(double a, double b, size_t N);

#endif //FEM_LAB_MATH_MODEL_HPP
