#include <iostream>
#include <sstream>
#include <cmath>
#include <algorithm>
#include "math_model.hpp"
#include "lin_elem_model.hpp"
#include "plotter.hpp"
#include "quad_elem_model.hpp"

double analytic_func(double x) {

//	long double c1_coeff = 7. * sqrt(3. / 2.) * exp(-sqrt(6)) / (7. + 7. * exp(28. * sqrt(6)));
//	c1_coeff +=(68. / (7. + 7. * exp(28. * sqrt(6.)))) * exp(13. * sqrt(6.));
//	long double c2_coeff = (68. * exp(15. * sqrt(6.)) - 7. * sqrt(3. / 2.) * exp(29. * sqrt(6.))) / (7. + 7. * exp(28. * sqrt(6.)));
//	return c1_coeff * exp(sqrt(2. / 3.) * x) + c2_coeff * exp(-sqrt(2. / 3.) * x) + 4. / 14.;
	return (2. * (-3. * exp(21. - (3. * x)/2.) - 5. * exp(39. - (3.*x)/2.) - 5. * exp((3. * x) / 2. - 3.) + 3. * exp((3.*x)/2. + 15.) + 5. + 5. * exp(36.))) /
	(9. * (1. + exp(36.)));
}

typedef double (*function)(double);

std::vector<double> vector_func(std::vector<double>& vector_x, function func) {
	std::vector<double> result;
	for (const auto& it: vector_x) {
		result.push_back(func(it));
	}
	return result;
}

double get_max_error(std::vector<double>& first, std::vector<double>& second) {
	if (first.size() != second.size()) {
		throw std::invalid_argument("sizes don't match");
	}
//	std::cout << "ERROR" << std::endl;
	double max_error = 0.;
	std::vector<double> errors(first.size());
	for (int i = 0; i < first.size(); ++i) {
		errors[i] = (first[i] - second[i]);
		if (std::abs(errors[i]) > max_error) {
			max_error = std::abs(errors[i]);
		}
//		std::cout << errors[i] << std::endl;
	}
//	std::cout << "-------------------" << std::endl;
	return max_error;
}

int main(int argc, char* argv[]) {
	if (argc < 2) {
		throw std::invalid_argument("usage: <prog_name> nodes_num");
	}
	std::stringstream str(argv[1]);
	int nodes_num = 0;
	str >> nodes_num;
	// linear
	std::shared_ptr<MathModel> linear_get_lae = std::make_shared<LinElemModel>(nodes_num);
	std::shared_ptr<LAESystem> linear_system = (*linear_get_lae)();
	auto linear_nodes = linear_get_lae->GetNodes();
//	linear_system->Print();
	linear_system->Solve();
	auto linear_elem_solution = linear_system->GetSolution();
	*linear_elem_solution = std::vector<double>(linear_elem_solution->begin(), linear_elem_solution->end() - 2);
//	std::cout << "LINEAR SOLUTION IS HERE:" << std::endl;
//	for (auto& elem : *linear_elem_solution) {
//		std::cout << elem << std::endl;
//	}

	// quadratic
	std::shared_ptr<MathModel> quad_get_lae = std::make_shared<QuadElemModel>(nodes_num);
	std::shared_ptr<LAESystem> quad_system = (*quad_get_lae)();
	auto quad_nodes = quad_get_lae->GetNodes();
//	quad_system->Print();
	quad_system->Solve();
	auto quad_elem_solution = quad_system->GetSolution();
	*quad_elem_solution = std::vector<double>(quad_elem_solution->begin(), quad_elem_solution->end() - 2);
//	std::cout << "QUADRATIC SOLUTION IS HERE:" << std::endl;
//	for (auto& elem : *quad_elem_solution) {
//		std::cout << elem << std::endl;
//	}

	// analytic
	auto analytic_solution = vector_func(linear_nodes, analytic_func);

	// errors
	std::cout << "error with linear element = " << get_max_error(analytic_solution, *linear_elem_solution) << std::endl;

	analytic_solution = vector_func(quad_nodes, analytic_func);

	std::cout << "error with quadratic element = " << get_max_error(analytic_solution, *quad_elem_solution) << std::endl;

	// plots
	Plotter plt;
	std::string name = "linear element";
	plt.AddPlot(*linear_elem_solution, linear_nodes, name);
	name = "analytic solution";
	plt.AddPlot(analytic_solution, linear_nodes, name);
	name = "quadratic element";
	plt.AddPlot(*quad_elem_solution, quad_nodes, name);
	plt.Show();
	return 0;
}