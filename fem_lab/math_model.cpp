//
// Created by sergey on 27.11.19.
//

#include <vector>

#include "math_model.hpp"

template <typename T>
std::vector<T> linspace(T a, T b, size_t N) {
	T h = (b - a) / static_cast<T>(N-1);
	std::vector<T> xs(N);
	typename std::vector<T>::iterator x;
	T val;
	for (x = xs.begin(), val = a; x != xs.end(); ++x, val += h)
		*x = val;
	return xs;
}

template<>
std::vector<double> linspace(double a, double b, size_t N) {
	double h = (b - a) / static_cast<double>(N-1);
	std::vector<double> xs(N);
	typename std::vector<double>::iterator x;
	double val;
	for (x = xs.begin(), val = a; x != xs.end(); ++x, val += h)
		*x = val;
	return xs;
}
